public static class Constants
{
    public const int INVENTORY_SLOTS = 9;

    public const string UI = nameof(UI);
    public const string Item = nameof(Item);

    public const string Hero = nameof(Hero);
    public const string Boss = nameof(Boss);

    public static class MouseAxes
    {
        public const string MouseX = "Mouse X";
        public const string MouseY = "Mouse Y";
    }

    public static class VirtualAxes
    {
        public const string Horizontal = nameof(Horizontal);
        public const string Vertical = nameof(Vertical);
    }

    public static class Tags
    {
        public const string Weapon = nameof(Weapon);
        public const string Hand = nameof(Hand);
    }

    public static class HeroBooleans
    {
        public const string IsWalking = nameof(IsWalking);
        public const string IsRunning = nameof(IsRunning);
    }

    public static class EnemyTriggers
    {
        public const string StandingMeleeAttack = nameof(StandingMeleeAttack);
        public const string LeftPunch = nameof(LeftPunch);
        public const string RightPunch = nameof(RightPunch);
        public const string ZombieDeathTrigger = nameof(ZombieDeathTrigger);
        public const string ZombieWalkingTrigger = nameof(ZombieWalkingTrigger);
        public const string ZombieAttackTrigger = nameof(ZombieAttackTrigger);
    }

    public static class HeroTriggers
    {
        public const string LeftPunchTrigger = nameof(LeftPunchTrigger);
        public const string MeleeAttackTrigger = nameof(MeleeAttackTrigger);
        public const string RightPunchTrigger = nameof(RightPunchTrigger);
        public const string JumpTrigger = nameof(JumpTrigger);
        public const string FlyDeathTrigger = nameof(FlyDeathTrigger);
    }
}
