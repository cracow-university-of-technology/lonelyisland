﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
	public Text DialogueText;

	private readonly Queue<string> _sentences;

	public DialogueManager()
	{
		_sentences = new Queue<string>();
	}

    void Update()
    {
		if (Input.GetKeyDown(KeyCode.F))
        {
			DisplaySentence();
        }
	}

	public void StartDialogue(Dialogue dialogue)
	{
		_sentences.Clear();

		foreach (var sentence in dialogue.Sentences)
		{
			_sentences.Enqueue(sentence);
		}

		DisplaySentence();
	}

    private void DisplaySentence()
	{
		if (_sentences.Count == 0)
		{
			return;
		}

		var sentence = _sentences.Dequeue();

		DialogueText.text = sentence;

		StopAllCoroutines();

		StartCoroutine(TypeSentence(sentence));
	}

	private IEnumerator TypeSentence(string sentence)
	{
		DialogueText.text = string.Empty;

		foreach (var letter in sentence.ToCharArray())
		{
			DialogueText.text += letter;

			yield return null;
		}
	}
}
