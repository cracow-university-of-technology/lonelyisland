﻿using UnityEngine;

using Random = System.Random;

public class WeaponStats : MonoBehaviour
{
    public int MinDamage = 0;
    public int MaxDamage = 0;
    
    public int GetDamage()
    {
        return new Random().Next(MinDamage, MaxDamage);
    }
}
