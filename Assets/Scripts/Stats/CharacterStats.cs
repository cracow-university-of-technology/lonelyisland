﻿using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public int Health = 100;

    public int CurrentHealth { get; set; }
    public bool IsDead { get; private set; }

    public int Damage;

    private AudioSource _audioSource;

    void Awake()
    {
        CurrentHealth = Health;

        _audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        // TODO: needs to be removed in the future
        
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(10);
        }
    }

    public void TakeDamage(int damage)
    {
        CurrentHealth -= damage;

        if (CurrentHealth <= 0 && !IsDead)
        {
            IsDead = true;

            _audioSource.Play();
            
            Die();
        }
    }

    public virtual void Die() { }
}
