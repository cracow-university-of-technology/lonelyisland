﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public float LookRadius;

    private Transform _enemyTarget;
    private NavMeshAgent _meshAgent;
    private CharacterCombat _characterCombat;

    public Animator EnemyAnimator;

    public EnemyController()
    {
        LookRadius = 10f;
    }

    void Start()
    {
        _enemyTarget = HeroManager.Instance._heroObject.transform;

        _meshAgent = GetComponent<NavMeshAgent>();
        _characterCombat = GetComponent<CharacterCombat>();
    }

    void Update()
    {
        if (!gameObject.GetComponent<EnemyFightingAnimationsAndStats>().IsDead)
        {
            var distance = Vector3.Distance(_enemyTarget.position, transform.position);

            if (distance <= LookRadius)
            {
                _meshAgent.SetDestination(_enemyTarget.position);

                EnemyAnimator.SetBool(Constants.EnemyTriggers.ZombieWalkingTrigger, true);

                if (distance <= _meshAgent.stoppingDistance)
                {
                    var targetStats = _enemyTarget.GetComponent<CharacterStats>();

                    if (targetStats != null)
                    {
                        _characterCombat.Attack(targetStats);

                        EnemyAnimator.SetTrigger(Constants.EnemyTriggers.ZombieAttackTrigger);
                    }

                    var direction = (_enemyTarget.position - transform.position).normalized;

                    var lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));

                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

                    EnemyAnimator.SetTrigger(Constants.EnemyTriggers.ZombieAttackTrigger);

                    EnemyAnimator.SetBool(Constants.EnemyTriggers.ZombieWalkingTrigger, false);
                }
            }
            else
            {
                EnemyAnimator.SetBool(Constants.EnemyTriggers.ZombieWalkingTrigger, false);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.position, LookRadius);
    }
}
