﻿using UnityEngine;

public class EnemyFightingAnimationsAndStats : MonoBehaviour
{
    private bool _damageWasDealt;

    private GameObject _heroObject;

    private CharacterStats _enemyStats;

    private Animator _heroAnimator;

    private AudioSource _audioSource;

    public bool IsDead { private set; get; }
    public bool IsBossDead { private set; get; }

    public Animator EnemyAnimator;

    public EnemyFightingAnimationsAndStats()
    {
        IsDead = false;
        IsBossDead = false;

        _damageWasDealt = false;
    }

    void Start()
    {
        _heroObject = GameObject.Find(Constants.Hero);

        _enemyStats = gameObject.GetComponent<CharacterStats>();

        _heroAnimator = _heroObject.GetComponent<HeroMovement>().HeroAnimator;

        _audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        _heroObject = GameObject.Find(Constants.Hero);

        if (_damageWasDealt)
        {
            return;
        }

        if (
            other.gameObject.tag == Constants.Tags.Weapon &&
            _heroAnimator.GetCurrentAnimatorStateInfo(0).IsName(Constants.EnemyTriggers.StandingMeleeAttack)
        )
        {
            _enemyStats.CurrentHealth -= other.gameObject.GetComponent<WeaponStats>().GetDamage();

            _damageWasDealt = true;
        }

        if (
            other.gameObject.tag == Constants.Tags.Hand &&
            _heroAnimator.GetCurrentAnimatorStateInfo(0).IsName(Constants.EnemyTriggers.LeftPunch)
        )
        {
            _enemyStats.CurrentHealth -= 2;

            _damageWasDealt = true;
        }

        if (
            other.gameObject.tag == Constants.Tags.Hand &&
            _heroAnimator.GetCurrentAnimatorStateInfo(0).IsName(Constants.EnemyTriggers.RightPunch)
        )
        {
            _enemyStats.CurrentHealth -= 3;

            _damageWasDealt = true;
        }
    }

    void Update()
    {
        if (_damageWasDealt &&
            (
                (
                    _heroAnimator.GetCurrentAnimatorStateInfo(0).IsName(Constants.EnemyTriggers.StandingMeleeAttack) &&
                    _heroAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.99f
                ) ||
                (
                    _heroAnimator.GetCurrentAnimatorStateInfo(0).IsName(Constants.EnemyTriggers.LeftPunch) &&
                    _heroAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.99f
                ) ||
                (
                    _heroAnimator.GetCurrentAnimatorStateInfo(0).IsName(Constants.EnemyTriggers.RightPunch) &&
                    _heroAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.99f
                )
            )
        )
        {
            _damageWasDealt = false;
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            _enemyStats.CurrentHealth -= 40;
        }

        if (_enemyStats.CurrentHealth <= 0 && !IsDead)
        {
            IsDead = true;

            EnemyAnimator.SetTrigger(Constants.EnemyTriggers.ZombieDeathTrigger);

            _heroObject.GetComponent<HeroFightingAnimationsAndStats>().AddScore(10);
            _heroObject.GetComponent<HeroFightingAnimationsAndStats>().AddKills();

            _audioSource.Play();

            if (gameObject.tag == Constants.Boss)
            {
                IsBossDead = true;
            }

            Invoke(nameof(DestroyEnemy), 120f);
        }
    }

    void DestroyEnemy()
    {
        Destroy(gameObject);
    }
}
