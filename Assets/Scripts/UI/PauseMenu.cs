﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool IsPaused;

    public GameObject HeroObject;
    public GameObject PauseMenuUI;

    public PauseMenu()
    {
        IsPaused = false;
    }

    void Awake()
    {
        PauseMenuUI.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HeroObject = GameObject.Find(Constants.Hero);

            if (IsPaused)
            {
                Resume();
            }
            else
            {
                if (!HeroObject.GetComponent<CharacterStats>().IsDead)
                {
                    Pause();
                }
            }
        }
    }

    public void Pause()
    {
        HeroFightingAnimationsAndStats.IsMouseBlocked = true;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        PauseMenuUI.SetActive(true);

        Time.timeScale = 0;

        IsPaused = true;
    }

    public void Resume()
    {
        HeroFightingAnimationsAndStats.IsMouseBlocked = false;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        PauseMenuUI.SetActive(false);

        Time.timeScale = 1;

        IsPaused = false;
    }

    public void MainMenu()
    {
        Time.timeScale = 1;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
