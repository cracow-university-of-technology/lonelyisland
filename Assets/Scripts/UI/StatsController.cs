﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class StatsController : MonoBehaviour
{
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI KillsText;

    public GameObject StatsCanvas;
    public GameObject HeroObject;
    
    private void Awake()
    {
        StatsCanvas.SetActive(false);
    }

    void Update()
    {
        HeroObject = GameObject.Find(Constants.Hero);

        if (HeroObject.GetComponent<CharacterStats>().IsDead)
        {
            InitializeStats();
        }
        else
        {
            StatsCanvas.SetActive(false);
        }

        if (GameObject.Find(Constants.Boss).GetComponent<EnemyFightingAnimationsAndStats>().IsBossDead)
        {
            InitializeStats();

            HeroObject.GetComponent<HeroMovement>().enabled = false;
            HeroObject.GetComponentInChildren<Camera>().GetComponent<HeroMouseMovement>().enabled = false;

            Invoke(nameof(RestartGame), 5.0f);
        }
    }

    public void InitializeStats()
    {
        ScoreText.SetText(HeroObject.GetComponent<HeroFightingAnimationsAndStats>().Score.ToString());
        KillsText.SetText(HeroObject.GetComponent<HeroFightingAnimationsAndStats>().Kills.ToString());

        StatsCanvas.SetActive(true);
    }

    void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
