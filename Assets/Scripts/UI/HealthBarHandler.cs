﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBarHandler : MonoBehaviour
{
    private float _currentHealth;
    private float _health;

    private Image _healthBar;

    public GameObject HeroObject;

    void Awake()
    {
        _health = HeroObject.GetComponent<HeroStats>().Health;
    }

    void Start()
    {
        _healthBar = GetComponent<Image>();
    }

    void Update()
    {
        HeroObject = GameObject.Find(Constants.Hero);

        _currentHealth = HeroObject.GetComponent<HeroStats>().CurrentHealth;

        _healthBar.fillAmount = _currentHealth / _health;
    }
}
