﻿using UnityEngine;

public class InventoryItem : MonoBehaviour, IInventoryItem
{
    public virtual string Name => null;

    [SerializeField]
    private Sprite _image;
    
    public Sprite Image => _image;

    [SerializeField]
    private Vector3 _pickUpPosition;

    [SerializeField]
    private Vector3 _pickUpRotation;

    public Vector3 PickUpPosition => _pickUpPosition;
    public Vector3 PickUpRotation => _pickUpRotation;

    public void OnPickup()
    {
        gameObject.SetActive(false);
    }

    public void OnUse(IInventoryItem currentItem)
    {
        Inventory.HasBusySlot = true;

        var newItemGameObject = gameObject;

        var newItemGameObjectPickUpHandler = gameObject.GetComponent<InventoryItemPickUpHandler>();

        newItemGameObjectPickUpHandler.IsEquipped = true;
        
        var currentItemGameObject = (currentItem as MonoBehaviour)?.gameObject;

        if (currentItemGameObject != null)
        {
            var currentItemPickUpHandler = currentItemGameObject.GetComponent<InventoryItemPickUpHandler>();

            if (newItemGameObject == currentItemGameObject)
            {
                currentItemPickUpHandler.IsEquipped = true;
            }
            else
            {
                currentItemPickUpHandler.IsEquipped = false;
            }

            currentItemGameObject.SetActive(false);
        }

        gameObject.SetActive(true);
    }
}
