﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static bool HasBusySlot;

    [NonSerialized]
    public bool IsOpened;

    private readonly List<IInventoryItem> _items;

    public GameObject InventoryUI;

    public IInventoryItem CurrentItem;

    public event EventHandler<InventoryEventArgs> OnItemAdd;
    public event EventHandler<InventoryEventArgs> OnItemRemove;

    void Awake()
    {
        InventoryUI.SetActive(false);
    }

    void Update()
    {
        if (!PauseMenu.IsPaused)
        {
            if (Input.GetKeyDown(KeyCode.J) && !IsOpened)
            {
                HeroFightingAnimationsAndStats.IsMouseBlocked = true;

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

                InventoryUI.SetActive(true);

                Time.timeScale = 0;

                IsOpened = true;
            }
            else if (Input.GetKeyDown(KeyCode.J) && IsOpened)
            {
                HeroFightingAnimationsAndStats.IsMouseBlocked = false;

                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;

                InventoryUI.SetActive(false);

                Time.timeScale = 1;

                IsOpened = false;
            }
        }
    }

    public Inventory()
    {
        _items = new List<IInventoryItem>();
    }

    public void AddItem(IInventoryItem item)
    {
        if (_items.Count < Constants.INVENTORY_SLOTS)
        {
            _items.Add(item);

            OnItemAdd?.Invoke(this, new InventoryEventArgs(item));
        }
    }

    public void RemoveItem(IInventoryItem item)
    {
        if (_items.Contains(item))
        {
            _items.Remove(item);

            OnItemRemove?.Invoke(this, new InventoryEventArgs(item));
        }
    }
}
