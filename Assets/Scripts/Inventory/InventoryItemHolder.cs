﻿using UnityEngine;

public class InventoryItemHolder : MonoBehaviour
{
    public IInventoryItem Item { get; set; }
}
