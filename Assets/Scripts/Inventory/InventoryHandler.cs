﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryHandler : MonoBehaviour
{
    public Inventory Inventory;

    void Start()
    {
        Inventory.OnItemAdd += OnItemAdd;

        Inventory.OnItemRemove += OnItemRemove;
    }
    
    private void OnItemAdd(object sender, InventoryEventArgs e)
    {
        var inventorySlots = transform.Find(Constants.UI);

        foreach (var inventorySlot in inventorySlots)
        {
            var imageTransform = (inventorySlot as Transform)?.GetChild(0).GetChild(0);

            var image = imageTransform.GetComponent<Image>();

            var itemHolder = imageTransform.GetComponent<InventoryItemHolder>();
            
            if (!image.enabled)
            {
                image.enabled = true;
                image.sprite = e.Item.Image;

                itemHolder.Item = e.Item;

                e.Item.OnPickup();

                break;
            }
        }
    }

    private void OnItemRemove(object sender, InventoryEventArgs e)
    {
        var inventorySlots = transform.Find(Constants.UI);

        foreach (var inventorySlot in inventorySlots)
        {
            var imageTransform = (inventorySlot as Transform)?.GetChild(0).GetChild(0);

            var image = imageTransform.GetComponent<Image>();

            var itemHolder = imageTransform.GetComponent<InventoryItemHolder>();
            
            if (itemHolder.Item?.Equals(e.Item) ?? default)
            {
                image.enabled = false;
                image.sprite = null;

                itemHolder.Item = null;

                break;
            }
        }
    }
}
