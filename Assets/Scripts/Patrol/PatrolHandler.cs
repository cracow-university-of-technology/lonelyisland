﻿using UnityEngine;

public class PatrolHandler : MonoBehaviour
{
    private int _currentWaypointIndex;
    private float _distance;

    public int Speed;

    public Transform[] Waypoints;

    void Start()
    {
        _currentWaypointIndex = 0;

        transform.LookAt(Waypoints[_currentWaypointIndex].position);
    }

    void Update()
    {
        _distance = Vector3.Distance(transform.position, Waypoints[_currentWaypointIndex].position);

        if (_distance < 1f)
        {
            _currentWaypointIndex++;
        
            if (_currentWaypointIndex >= Waypoints.Length)
            {
                _currentWaypointIndex = 0;

            }

            transform.LookAt(Waypoints[_currentWaypointIndex].position);
        }

        Patrol();
    }

    void Patrol()
    {
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
    }
}
