﻿using UnityEngine;

public class HeroFightingAnimationsAndStats : MonoBehaviour
{
    public static bool IsMouseBlocked;

    public int Score { private set; get; }
    public int Kills { private set; get; }

    public void AddScore(int score)
    {
        Score += score;
    }

    public void AddKills()
    {
        Kills++;
    }

    public HeroFightingAnimationsAndStats()
    {
        IsMouseBlocked = false;

        Score = 0;
    }

    void Update()
    {
        if (!IsMouseBlocked)
        {
            if (Input.GetMouseButtonDown(0))
            {
                gameObject.GetComponent<HeroMovement>().HeroAnimator.SetTrigger(Constants.HeroTriggers.LeftPunchTrigger);
            }

            if (Inventory.HasBusySlot)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    gameObject.GetComponent<HeroMovement>().HeroAnimator.SetTrigger(Constants.HeroTriggers.MeleeAttackTrigger);
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(1))
                {
                    gameObject.GetComponent<HeroMovement>().HeroAnimator.SetTrigger(Constants.HeroTriggers.RightPunchTrigger);
                }
            }
        }
    }
}
