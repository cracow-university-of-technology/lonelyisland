﻿using UnityEngine;

public class HeroMouseMovement : MonoBehaviour
{
    private float _rotationX;

    public float MouseSensitivity;

    public Transform HeroTransform;

    public HeroMouseMovement()
    {
        _rotationX = 0f;

        MouseSensitivity = 100f;
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        var mouseX = Input.GetAxis(Constants.MouseAxes.MouseX) * MouseSensitivity * Time.deltaTime;
        var mouseY = Input.GetAxis(Constants.MouseAxes.MouseY) * MouseSensitivity * Time.deltaTime;

        _rotationX -= mouseY;

        _rotationX = Mathf.Clamp(_rotationX, -90f, 90f);

        transform.localRotation = Quaternion.Euler(_rotationX, 0f, 0f);

        HeroTransform.Rotate(Vector3.up * mouseX);
    }
}
