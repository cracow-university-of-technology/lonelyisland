﻿using UnityEngine;

public class HeroMovement : MonoBehaviour
{
    private bool _isGrounded;
    private bool _isUnderWater;
    private float _playerSpeed;

    private Vector3 _velocity;

    public float PlayerWalkSpeed;
    public float PlayerRunSpeed;
    public float Gravity;
    public float SphereRadius;
    public float JumpHeight;

    public Animator HeroAnimator;
    public Transform GroundCheckObject;
    public LayerMask GroundLayerMask;
    public CharacterController HeroCharacterController;

    public HeroMovement()
    {
        PlayerWalkSpeed = 2f;
        PlayerRunSpeed = 6f;

        _playerSpeed = PlayerWalkSpeed;
        _isUnderWater = false;

        Gravity = -9.81f;
        SphereRadius = 0.4f;
        JumpHeight = 3f;
    }

    void Start()
    {
        InvokeRepeating("Sink", 2.0f, 1.5f);
    }

    void Update()
    {
        _isGrounded = Physics.CheckSphere(GroundCheckObject.position, SphereRadius, GroundLayerMask);

        if (_isGrounded && _velocity.y < 0)
        {
            _velocity.y = -2f;
        }

        var horizontal = Input.GetAxis(Constants.VirtualAxes.Horizontal);
        var vertical = Input.GetAxis(Constants.VirtualAxes.Vertical);

        Vector3 movement = transform.right * horizontal + transform.forward * vertical;

        if (Input.GetKey(KeyCode.LeftShift) && movement.magnitude > 0)
        {
            _playerSpeed = PlayerRunSpeed;

            HeroAnimator.SetBool(Constants.HeroBooleans.IsWalking, false);
            HeroAnimator.SetBool(Constants.HeroBooleans.IsRunning, true);
        }
        else
        {
            _playerSpeed = PlayerWalkSpeed;

            if (movement.magnitude == 0)
            {
                HeroAnimator.SetBool(Constants.HeroBooleans.IsWalking, false);
                HeroAnimator.SetBool(Constants.HeroBooleans.IsRunning, false);
            }
            else
            {
                HeroAnimator.SetBool(Constants.HeroBooleans.IsWalking, true);
                HeroAnimator.SetBool(Constants.HeroBooleans.IsRunning, false);
            }
        }

        HeroCharacterController.Move(movement * _playerSpeed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space) && _isGrounded)
        {
            HeroAnimator.SetTrigger(Constants.HeroTriggers.JumpTrigger);

            _velocity.y = Mathf.Sqrt(JumpHeight * -2f * Gravity);
        }

        _velocity.y += Gravity * Time.deltaTime;

        HeroCharacterController.Move(_velocity * Time.deltaTime);

        if (gameObject.transform.position.y <= 3.30)
        {
            _isUnderWater = true;
        }
        else
        {
            _isUnderWater = false;
        }
    }

    void Sink()
    {
        if (_isUnderWater)
        {
            gameObject.GetComponent<CharacterStats>().TakeDamage(15);
        }
    }
}
