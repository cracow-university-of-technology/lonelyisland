﻿using UnityEngine;

public class HeroStats : CharacterStats
{
    public Animator HeroAnimator;

    public override void Die()
    {
        base.Die();

        HeroAnimator.SetTrigger(Constants.HeroTriggers.FlyDeathTrigger);

        gameObject.GetComponent<HeroMovement>().enabled = false;
        gameObject.GetComponentInChildren<Camera>().GetComponent<HeroMouseMovement>().enabled = false;

        Invoke(nameof(RestartGame), 5.0f);
    }

    void RestartGame()
    {
        HeroManager.Instance.RestartGame();
    }
}
