﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
    private bool _wasInteracted;
    private bool _isTextActive;

    public float InteractionRadius;
    
    public Dialogue Dialogue;
    public Transform InteractionTransform;

    private Transform _hero;
    
    public GameObject InteractionText;

    public Interactable()
    {
        _wasInteracted = false;
        _isTextActive = false;

        InteractionRadius = 3f;
    }

    public virtual void Interact() { }

    void OnDrawGizmosSelected()
    {
        if (InteractionTransform == null)
        {
            InteractionTransform = transform;
        }

        Gizmos.color = Color.yellow;
        
        Gizmos.DrawWireSphere(InteractionTransform.position, InteractionRadius);
    }

    void Start()
    {
        InteractionText.SetActive(_isTextActive);
    }

    void Update()
    {
        _hero = GameObject.Find(Constants.Hero).transform;

        if (!_wasInteracted)
        {
            var distance = Vector3.Distance(_hero.position, InteractionTransform.position);

            if (distance <= InteractionRadius)
            {
                _isTextActive = true;

                InteractionText.SetActive(_isTextActive);
                
                if (Input.GetKeyDown(KeyCode.F))
                {
                    Interact();

                    _wasInteracted = true;

                    InteractionText.SetActive(_isTextActive);

                    FindObjectOfType<DialogueManager>().StartDialogue(Dialogue);
                    
                    _isTextActive = false;
                }
            }

            if (distance > InteractionRadius && _isTextActive)
            {
                _isTextActive = false;

                InteractionText.SetActive(_isTextActive);
            }
        }

        if (_wasInteracted)
        {
            var distance = Vector3.Distance(_hero.position, InteractionTransform.position);

            if (distance > InteractionRadius)
            {
                InteractionText.SetActive(false);

                Interact();

                _wasInteracted = false;
            }

            if (distance > InteractionRadius && _isTextActive)
            {
                _isTextActive = false;

                InteractionText.SetActive(_isTextActive);
            }
        }
    }
}
